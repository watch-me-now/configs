# configs

Basic configuration files

## Demo

```bash
manager: 192.168.99.145
worker1: 192.168.99.146
worker2: 192.168.99.147

192.168.99.145/adminer
192.168.99.145:8081 - visualizer
192.168.99.145:8001 - json kong
192.168.99.145:9090 - prometheus
192.168.99.145:3000 - grafana
192.168.99.145:9000 - portainer
```

```bash
docker@myvm1:~$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
p725avfkj22ew4ivf21g8z3wa *   myvm1               Ready               Active              Leader              19.03.12
djkte3nd0mftfzmyrrr6ejy3f     myvm2               Ready               Active                                  19.03.12
ljnt6q80sxss1352i47drzont     myvm3               Ready               Active                                  19.03.12
```

```bash
docker@myvm1:~$ docker service ls
ID                  NAME                       MODE                REPLICAS               IMAGE                                                    PORTS
j5gk1q1zetlb        proiect01_adminer          replicated          1/1                    adminer:latest                                           *:8080->8080/tcp
yonty8ksk0w8        proiect01_db               replicated          1/1                    postgres:12                                              
itcewg2yyuu8        proiect01_grafana          replicated          1/1                    grafana/grafana:latest                                   *:3000->3000/tcp
tlwb7ddk9hnq        proiect01_io-service       replicated          3/3 (max 2 per node)   registry.gitlab.com/watch-me-now/io-service:latest       
8yjtsekirqwa        proiect01_kong             replicated          1/1                    kong:latest                                              *:80->8000/tcp, *:443->8443/tcp, *:8001->8001/tcp, *:8444->8444/tcp
mxmqvn4ha9zy        proiect01_movies-service   replicated          2/2 (max 1 per node)   registry.gitlab.com/watch-me-now/movies-service:latest   
yserftlahwur        proiect01_prometheus       replicated          1/1                    prom/prometheus:latest                                   *:9090->9090/tcp
pzekww3ezydg        proiect01_visualizer       replicated          1/1                    dockersamples/visualizer:stable                          *:8081->8080/tcp
```

```bash
docker@myvm1:~$ docker stack ps proiect01
ID                  NAME                         IMAGE                                                    NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
ai768dgoqd34        proiect01_visualizer.1       dockersamples/visualizer:stable                          myvm1               Running             Running 18 minutes ago                       
pnbp4pormidk        proiect01_adminer.1          adminer:latest                                           myvm2               Running             Running 19 minutes ago                       
ldecw2vs342x        proiect01_db.1               postgres:12                                              myvm1               Running             Running 18 minutes ago                       
tp0c3zj8xwpb        proiect01_io-service.1       registry.gitlab.com/watch-me-now/io-service:latest       myvm2               Running             Running 19 minutes ago                       
hyr4p0nxz8zk        proiect01_movies-service.1   registry.gitlab.com/watch-me-now/movies-service:latest   myvm2               Running             Running 19 minutes ago                       
o4oub9emn1jm        proiect01_prometheus.1       prom/prometheus:latest                                   myvm1               Running             Running 19 minutes ago                       
nw871ffqne8a        proiect01_grafana.1          grafana/grafana:latest                                   myvm1               Running             Running 19 minutes ago                       
xndhaxugs1fh        proiect01_kong.1             kong:latest                                              myvm1               Running             Running 19 minutes ago                       
4obgy72lehyc        proiect01_io-service.2       registry.gitlab.com/watch-me-now/io-service:latest       myvm3               Running             Running 19 minutes ago                       
yrcb214qa5gs        proiect01_movies-service.2   registry.gitlab.com/watch-me-now/movies-service:latest   myvm3               Running             Running 19 minutes ago                       
s1u9rx38gvs1        proiect01_io-service.3       registry.gitlab.com/watch-me-now/io-service:latest       myvm1               Running             Running 18 minutes ago   
```

```bash
#ci/cd
docker run -d --name gitlab-runner --restart always -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register

sudo docker restart gitlab-runner
```

