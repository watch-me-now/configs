docker-machine scp -r database manager:.
docker-machine scp -r kong manager:.
docker-machine scp -r prometheus manager:.
docker-machine scp docker-compose.yml manager:.
docker-machine scp portainer.yml manager:.
